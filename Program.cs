﻿using System;

namespace comp5002_10011594_assessment01
{
    class Program
    {
        static void Main(string[] args)
        
        {   
            {
                // display the welcome message
                Console.WriteLine("======================");
                Console.WriteLine("=Welcome to the store=");
                Console.WriteLine("======================");

                // ask for the users name and place into the variable 'name'
                Console.WriteLine("Please input your name, then press <ENTER>");
                var name = Console.ReadLine();
                // ask the user for the item price and store into the variable 'items'
                Console.WriteLine($"Now {name} please enter the price of your");
                Console.WriteLine("item to two decimal places, then press enter");
                var item = double.Parse(Console.ReadLine());
                // ask if the user wants to add an extra item input into variable 'decision'
                Console.WriteLine($"{name} if you would like to add another item");
                Console.WriteLine("please type yes or no for the corresponding action");
                var decision = Console.ReadLine();
                // if statement 'yes' got to the first code block if 'no' 
                // or anything else go to the second code block 
                if (decision == "yes" || decision == "Yes") 
                {
                    // first code block two items with and without GST
                    // add second item into new variable 'items'
                    Console.WriteLine($"Now {name} please enter the price of your second");
                    Console.WriteLine("item to two decimal places, then press enter");
                    item += double.Parse(Console.ReadLine());
                    Console.WriteLine("This is your total excluding GST");
                    Console.WriteLine($"{item:C2}");
                    Console.WriteLine("This is your total including GST");
                    Console.WriteLine($"{item * 1.15:C2}");
                }
                else
                {
                    // second code block one item with and without GST    
                    Console.WriteLine("This is your total excluding GST");
                    Console.WriteLine($"{item:C2}");
                    Console.WriteLine("This is your total including GST");
                    Console.WriteLine($"{item * 1.15:C2}");
                }
                // leaving message
                Console.WriteLine($"Thank you for shopping with us {name}, please come again");
                // End the program with enter
                Console.WriteLine("Press <ENTER> to finnish shopping");
                Console.ReadKey();
            }
        }
    }
}

